BukkitScan
==========
The Black Box Minecraft Server Scanner [By Joshumax]

Disclaimer:
-----------
```
Bukkitscan is meant to be used legally for security purposes only.
DO *NOT* USE BUKKITSCAN ON ANY SERVER THAT YOU DO NOT HAVE PERMISSION TO SCAN.

The authors and contributors of this program are in NO way responsible for the
poor actions of you, your friend, or your pet gerbil. USE THIS SOFTWARE AT
YOUR OWN RISK. Got that, script kiddies? Move along now, nothing to see here...

With that out of the way...
```

About:
------
BukkitScan is a customizable, scriptable black-box security scanner for Minecraft servers similar to
Bukkit and Spigot. This progam behaves much like the beloved wordpress scanner WPScan, but instead of
scanning for vulnerable wordpress websites, it scans for vulnerable or compromised Minecraft servers.

Getting Started:
----------------
Scanning a server using BukkitScan is easy, simply open a console or command prompt window and type:
```
java -jar BukkitScan.jar (IP):(PORT)
```
assuming that you have first downloaded and extracted a release archive and set the proper user details
in the supplied userpass.txt. More info on how to set up BukkitScan can be found at the GitHub wiki.

Commands:
---------
A full list of supported command-line arguments can be found below:
```
--enable-script=100_*           Enable custom script runner
--disable-script=99_*           Disable default script runner
--no-info                       Do not show any [INFO]
--enumerate=(p,u)               Enumerate plugins (p) or players (u)
--interval=X                    Set the command sending interval (In Millisecs)
--max-timeout=X                 Set the command response timeout (In Millisecs)
--error-regex=REGEX             Change the "Command not found" regex
--permission-regex=REGEX        Change the "Permission denied" regex
--offline-mode                  Connect to the server using offline mode
```

Releases:
---------
Release tags of BukkitScan are created as such that each version follows the template:
```
BS_MAJOR.MINOR.PATCH-MC.PROTOCOL.VERSION
```
i.e. release 1.0.0 of this software using MC protocol version 1.7.9 would be:
```
BS_1.0.0-1.7.9
```

Customization:
--------------
While BukkitScan comes with all of the necessary scripts to scan most generic Minecraft servers, sometimes it's necessary to add or remove custom script runners from BukkitScan for a more specific and enhanced scan. BukkitScan makes doing so extremely easy, simply add a new script to the /data directory using the specified format below
to cusomize your scanning session.
```
(LOAD_NUMBER)_(UNIQUE_NAME).script
```
LOAD_NUMBER defines exactly when a script should be executed during a scan. The lower the number, the sooner it is preformed as compared to a script with a higher number. I.e. 41_CheckSomeSettings.script will run before 42_CheckSomeMoreSettings.script. BukkitScan scripts with load numbers between 0 and 99 are automatically added to the scan queue, and must be disabled during a scan by either renaming them, or by
manually disabling them using --disable-script. Load numbers starting with 100+ are interpreted as optional load scripts which are not added to the queue by default, and can be enabled during a scan through --enable-script. This is useful if you have a specific collection of scipts that you only want
to use during certain scans. 101_SpigotOnly.script could simply be enabled during a Spigot server scan by
adding --enable-script=101_SpigotOnly to the scan argument list.

Scripting:
----------
Scipting for BukkitScan is fun and EASY. Simply create a new script as described in the "Customization"
section and create some CSV data telling what BukkitScan should do:
```
CSV Line Layout:

PLUGIN_NAME, COMMAND_TO_RUN, SUCCESS_REGEX, FAILURE_REGEX, SEVERITY (0-3), INFO_TEXT, VERSION (0 if none)

PLUGIN_NAME = The name of the plugin that you are testing for vulnerabilities

COMMAND_TO_RUN = The command string that you want to run to test for vulnerabilities. Entering (player) anywhere in this section will replace (player) with the player username.

SUCCESS_REGEX = This is the regex to test the server response to. If the response matches this, the plugin is deemed vulnerable by BukkitScan, and INFO_TEXT is displayed to the user, as well as other data. You can use "*" or (player) in this field, as well as many other JAVA regexes.

FAILURE_REGEX = This is checked before SUCCESS_REGEX. If the response matches this field, BukkitScan discards the response and assums that this line is not vulnerable. You can use "*" or (player) in this field, as well as many other JAVA regexes.

SEVERITY = The severity of this vulnerabilty, ranging from 0 to 3. 0 = INFO, 1 = LOW, 2 = Medium, 3 = HIGH.

INFO_TEXT = The accompanying text to show if a vulnerability is found in the line.

VERSION = The version of the plugin that is vulnerabile to exploitation. Use 0 it does not have a version or the version is unknown.
```
Example:
```
"Builtin","/tp (player) (player)","*Teleported*","*",2,"Player can use /tp to teleport",0
```
TODO: ADD MORE SCRIPTING DOCS

Known Bugs:
-----------
* Trouble handling multi-line responses (WIP)
* Concurrency Exceptions (WIP)

Contributing:
-------------
Since BukkitScan is very much in the development stage, contributions are highly important to the overall
well-being of this project. If you think you can fix up a bug or two, add a cool new feature, or even add
some much-needed documentation. Please send me a pull request.

Donate:
-------
SOON.