package com.bukkitsecurity.bukkitscan;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.AnsiOutputStream;

/**
 * Logger class
 */
public class Logger {

    // Display info?
    public static Boolean display_info = true;

    // Log levels
    public static final int LOG_INFO = 0;
    public static final int LOG_LOW = 1;
    public static final int LOG_WARN = 2;
    public static final int LOG_SEVERE = 3;
    public static final int LOG_CRIT = 4;

    // Logger
    public static void log(int severity, String details) {
        String output;

        switch (severity) {
            case LOG_INFO:
                if (!display_info)
                    return;
                output = "@|white [INFO] - " + details + "|@";
                break;
            case LOG_LOW:
                output = "@|green [LOW] - " + details + "|@";
                break;
            case LOG_WARN:
                output = "@|yellow [MED] - " + details + "|@";
                break;
            case LOG_SEVERE:
                output = "@|red [HIGH] - " + details + "|@";
                break;
            case LOG_CRIT:
                // Reserved for app only
                output = "@|red [ERROR] - " + details + "|@";
                break;
            default:
                output = "[*] " + details;
                break;
        }

        // Print ANSI
        Ansi ansi = new Ansi();
        AnsiConsole.out.println(ansi.render(output));
    }
}
