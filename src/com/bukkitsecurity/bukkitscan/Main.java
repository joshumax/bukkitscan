package com.bukkitsecurity.bukkitscan;

import com.sun.xml.internal.bind.v2.TODO;

import java.util.*;

public class Main {

    // Display the really flashy title
    private static void display_title() {
        System.out.println("_____________________________________________________________________________");
        System.out.println("" +
                            " ____  _    _ _  ___  _______ _______ _____  _____          _   _\n" +
                            "|  _ \\| |  | | |/ / |/ /_   _|__   __/ ____|/ ____|   /\\   | \\ | |\n" +
                            "| |_) | |  | | ' /| ' /  | |    | | | (___ | |       /  \\  |  \\| |\n" +
                            "|  _ <| |  | |  < |  <   | |    | |  \\___ \\| |      / /\\ \\ | . ` |\n" +
                            "| |_) | |__| | . \\| . \\ _| |_   | |  ____) | |____ / ____ \\| |\\  |\n" +
                            "|____/ \\____/|_|\\_\\_|\\_\\_____|  |_| |_____/ \\_____/_/    \\_\\_| \\_|");
        System.out.println("BukkitScan version " + Version.id + ". To be used legally for security purposes ONLY.");
        System.out.println("_____________________________________________________________________________\n");
    }

    // Show usage
    private static void show_usage() {
        System.out.println("Usage:");
        System.out.println("\tbukkitscan.jar SERVER_IP ARGS\n");
        System.out.println("Arguments:");
        System.out.println("\t--enable-script=100_*\t\tEnable custom script runner");
        System.out.println("\t--disable-script=99_*\t\tDisable default script runner");
        System.out.println("\t--no-info\t\t\tDo not show any [INFO]");
        System.out.println("\t--enumerate=(p,u)\t\tEnumerate plugins (p) or players (u)");
        System.out.println("\t--interval=X\t\t\tSet the command sending interval (In Millisecs)");
        System.out.println("\t--max-timeout=X\t\t\tSet the command response timeout (In Millisecs)");
        System.out.println("\t--error-regex=REGEX\t\tChange the \"Command not found\" regex");
        System.out.println("\t--permission-regex=REGEX\tChange the \"Permission denied\" regex");
        System.out.println("\t--offline-mode\t\t\tConnect to the server using offline mode\n");
        System.out.println("Example:");
        System.out.println("\tbukkitscan 127.0.0.1:25565 --enable-script=102_myscript\n");
        System.exit(1);
    }

    // This should probably be re-done in the future
    private static List<String> parse_args(String[] args) {
        // Loop and collect n args after server arg
        List<String> argarray = new ArrayList<String>();
        for (int count = 1; count < args.length; count++) {
            String[] argsplit = args[count].split("=");

            // Derp
            if (argsplit[0].matches("--no-info")) {
                // Do not display info
                Logger.display_info = false;
                continue;
            }
            if (argsplit[0].matches("--offline-mode")) {
                // Use offline mode authentication
                Credentials.offline = true;
                continue;
            }

            // Check length for next options
            if (argsplit.length <= 1) {
                System.out.println(argsplit[0] + ": Missing parameter");
                System.exit(1);
            }
            if (argsplit[0].matches("--enable-script|--disable-script")) {
                argarray.add(argsplit[1]);
                continue;
            }
            if (argsplit[0].matches("--enumerate")) {
                // TODO run enumerate functions
                System.out.println("Not implemented :(");
                System.exit(1);
                continue;
            }
            if (argsplit[0].matches("--interval")) {
                // Set command sending interval
                try {
                    Scanner.interval = Integer.parseInt(argsplit[1]);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid number");
                }
                continue;
            }
            if (argsplit[0].matches("--max-timeout")) {
                // Set command max timeout period
                try {
                    Scanner.timeout = Integer.parseInt(argsplit[1]);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid number");
                }
                continue;
            }
            if (argsplit[0].matches("--error-regex")) {
                Scanner.error_regex = Scanner.replace_regex_literals(argsplit[1]);
                continue;
            }
            if (argsplit[0].matches("--permission-regex")) {
                Scanner.permission_regex = Scanner.replace_regex_literals(argsplit[1]);
                continue;
            }

            // Unknown argument
            System.out.println("Unknown argument: " + argsplit[0]);
            System.exit(1);
        }
        return argarray;
    }

    // Parse the server args (i.e. 127.0.0.1:25565 becomes (0)127.0.0.1 (1)25565
    private static void parse_server(String serverwithport, Scanner scanner) {
        if (!serverwithport.matches("(.*):(.*)")) {
            scanner.loadServer(serverwithport, 25565);
            return;
        }
        try {
            String split[] = serverwithport.split(":");
            int port = Integer.parseInt(split[1]);
            scanner.loadServer(split[0], port);
            return;
        } catch (NumberFormatException e) {
            System.out.println("Error parsing server " + serverwithport);
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        Script script_parser = new Script();
        Scanner scanner = new Scanner();

        // Title
        display_title();

        // Show help and usage
        if (args.length == 0 || args[0].matches("--help|-h"))
            show_usage();

        // Load scripts
        script_parser.load(parse_args(args));

        // Parse the server args
        parse_server(args[0], scanner);

        // Run pre-authentication checks
        // scanner.run_prechecks(); TODO

        // Initialize scanner
        scanner.initialize();

        // Run scripts and scan
        script_parser.run(scanner);

        // Shutdown
        scanner.deinitialize();

        System.out.println("\nThanks for playing! Have fun!");
    }
}
