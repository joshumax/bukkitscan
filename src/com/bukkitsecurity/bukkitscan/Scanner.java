package com.bukkitsecurity.bukkitscan;

import java.util.*;

/**
 * Bukkit scanner
 */
public class Scanner {

    // Default command sending interval
    public static int interval = 1000;

    // Default command timeout interval
    public static int timeout = 2000;

    // Command not found regular expression (Tested at runtime too)
    public static String error_regex = "";

    // Permission denied regex (Generic default)
    public static String permission_regex = replace_regex_literals("*ermiss*|*orry*");

    // Client instance
    private BSClient client = null;

    // Scanner errors
    public static int errors = 0;

    // Server and port string
    public static String server;
    public static int port = 25565; // Default port

    // Initialized
    private Boolean initialized = false;

    // Replace static regex literals "*" and "(player)" their respective substitutions
    public static String replace_regex_literals(String orig) {
        String new_string = "";

        // Replace "*" with java regex
        new_string = orig.replace("*", "(.*)");

        // Replace "(player)" with username
        new_string = new_string.replace("(player)", Credentials.getUsername());

        return new_string;
    }

    public static String replace_command_literals(String orig) {
        return orig.replace("(player)", Credentials.getUsername());
    }

    // Sets the server host and port
    public void loadServer(String server_arg, int port_arg) {
        // Set server host and port
        server = server_arg;
        port = port_arg;

        // Display
        Logger.log(Logger.LOG_INFO, "Set server to " + server_arg);
    }

    public void initialize() {
        // Already initialized
        if (initialized) {
            Logger.log(Logger.LOG_CRIT, "Already initialized on initialize()");
            System.exit(1);
        }

        // Instantiate client
        client = new BSClient();

        // Authenticate the user with the server using supplied credentials
        if (!Credentials.offline) {
            if (client.authenticateOnline(Credentials.getUsername(), Credentials.getPassword())) {
                Logger.log(Logger.LOG_INFO, "Authenticated with the auth server!");
            } else {
                Logger.log(Logger.LOG_CRIT, "Error authenticating with the auth server!");
                System.exit(1);
            }
        } else {
            client.authenticateOffline(Credentials.getUsername());
        }

        // Connect to the minecraft server
        if (!client.connect(server, port)) {
            Logger.log(Logger.LOG_CRIT, "connect()");
            System.exit(1);
        }

        // Sleep a bit so the server finishes setting up
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Done!
        Logger.log(Logger.LOG_INFO, "Connected to the server!");

        // Find the error regex
        find_error_regex();

        initialized = true;
    }

    public void deinitialize() {
        client.disconnect();
        client.logout();

        client = null;
        initialized = false;
    }

    // Find the server's "Unknown command" string by sending a few random commands
    private void find_error_regex() {
        // Check if the user already set the error regex
        if (!error_regex.equals("")) {
            Logger.log(Logger.LOG_INFO, "Skipping command error discovery...");
            error_regex = replace_regex_literals(error_regex);
            return;
        }

        // Create a random command
        String random_command = "/" + UUID.randomUUID().toString().split("-")[0];

        String response = client.sendCommand(random_command);

        if (response.equals("")) {
            Logger.log(Logger.LOG_CRIT, "Could not detect error regex, please set it manually.");
            System.exit(1);
        }

        // Set the response as the server's command not found string
        error_regex = response;
        Logger.log(Logger.LOG_INFO, "Set command error regex to \"" + response + "\"");

        // Sleep some
        try {
            Thread.sleep(interval);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void scan(String name, String command,
                     String success, String failure,
                     String level, String message,
                     String version) {
        // Check if we're initialized
        if (!initialized) {
            Logger.log(Logger.LOG_CRIT, "Scanner not initialized yet!");
            System.exit(1);
        }

        // Send the command to the server and store the result
        String result = client.sendCommand(replace_command_literals(command));

        String success_regex = replace_regex_literals(success);
        String failure_regex = "";

        // Don't use the regex if it's only a "*" (As it is a placeholder only)
        if (!failure.equals("*"))
            failure_regex = "|" + replace_regex_literals(failure); // Concat failure

        // Check to see if the result matches error_regex, permission_regex, or failure_regex
        if (!result.matches(error_regex + "|" + permission_regex + failure_regex) && !result.equals("")) {
            // Result matches, one more test for a positive match!
            if (result.matches(success_regex)) {
                // MATCH!! Display security warning to user
                if (!version.equals("0")) {
                    // Display w/ version
                    Logger.log(Integer.parseInt(level), name + " version " + version + ": " + message);
                } else {
                    // Display w/o version
                    Logger.log(Integer.parseInt(level), name + ": " + message);
                }

                // Sleep
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // Sleep a specific amount of time to delay sending commands
        try {
            Thread.sleep(interval);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}