package com.bukkitsecurity.bukkitscan;

import java.io.*;

/**
 * Gets credentials from userpass.txt
 */
public class Credentials {
    private static String username;
    private static String password;

    // Set to true for offline mode authentication
    public static Boolean offline = false;

    // Gets either line 1 (username) or line 2 (password)
    private static String getUserPassLine(int line) {
        if (line > 2) {
            System.out.println("getUserPassLine()");
            System.exit(1);
        }
        try {
            FileInputStream fs = new FileInputStream("userpass.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fs));
            try {
                for (int i = 0; i < line; ++i)
                    br.readLine();
                return br.readLine();
            } catch (Exception e) {
                System.out.println("Error reading userpass.txt");
            }
        } catch (FileNotFoundException e) {
            System.out.println("userpass.txt not found!");
            System.exit(1);
        }
        return "";
    }

    // Get Username
    public static String getUsername() {
        if (username == null)
            return getUserPassLine(0);
        return username;
    }

    // Get Password
    public static String getPassword() {
        if (username == null)
            return getUserPassLine(1);
        return username;
    }
}
