package com.bukkitsecurity.bukkitscan;

import org.spacehq.mc.auth.exception.AuthenticationException;
import org.spacehq.mc.protocol.MinecraftProtocol;
import org.spacehq.mc.protocol.data.message.Message;
import org.spacehq.mc.protocol.packet.ingame.client.ClientChatPacket;
import org.spacehq.mc.protocol.packet.ingame.server.ServerChatPacket;
import org.spacehq.packetlib.Client;
import org.spacehq.packetlib.event.session.DisconnectedEvent;
import org.spacehq.packetlib.event.session.PacketReceivedEvent;
import org.spacehq.packetlib.event.session.SessionAdapter;
import org.spacehq.packetlib.event.session.SessionListener;
import org.spacehq.packetlib.tcp.TcpSessionFactory;

/**
 * BukkitScan minecraft "client" used to send commands
 * And recieve the responses
 */
public class BSClient {
    // Protocol handler
    private MinecraftProtocol protocol = null;

    // MC client
    private Client client = null;

    // Authentication lock
    private Boolean authenticated = false;

    // Response string
    private String response = "";

    // Authenticate the user with an offline mode server (true on success, false if failed)
    public Boolean authenticateOffline(String username) {
        // Check auth status
        if (authenticated)
            return false;

        protocol = new MinecraftProtocol(username);
        authenticated = true;
        return true;
    }

    // Authenticate the user with the server (true on success, false if failed)
    public Boolean authenticateOnline(String username, String password) {
        // Check auth status
        if (authenticated)
            return false;

        try {
            protocol = new MinecraftProtocol(username, password, false);
        } catch(AuthenticationException e) {
            return false;
        }
        authenticated = true;
        return true;
    }

    public Boolean connect(String host, int port) {
        if (client != null && client.getSession().isConnected())
            return false;

        if (protocol == null)
            return false;

        // Connect to the server
        client = new Client(host, port, protocol, new TcpSessionFactory());
        client.getSession().connect();

        return true;
    }

    public Boolean disconnect() {
        if (client != null && !client.getSession().isConnected())
            return false;

        // Disconnect from the server
        client.getSession().disconnect("Finished");
        client = null;

        return true;
    }

    // De-authenticate with the auth server (true if success, false otherwise)
    public Boolean logout() {
        // Check auth status
        if (!authenticated)
            return false;

        // End
        authenticated = false;
        return true;
    }

    // Mutilate listeners
    private void remove_listeners() {
        for (SessionListener listener : client.getSession().getListeners())
            client.getSession().removeListener(listener);
    }

    public String sendCommand(String command) {
        if (client != null && !client.getSession().isConnected()) {
            Logger.log(Logger.LOG_CRIT, "Not connected to a server during sendCommand()!");
            System.exit(1);
        }

        if (command.equals(""))
            return ""; // Ignore bogus commands

        client.getSession().addListener(new SessionAdapter() {
            @Override
            public void packetReceived(PacketReceivedEvent event) {
                if (event.getPacket() instanceof ServerChatPacket) {
                    String chatstr = event.<ServerChatPacket>getPacket().getMessage().getFullText();
                    if (!chatstr.matches("(.*)joined the game.")) { // Filter out join packets
                        response = chatstr;
                    }
                }
            }

            @Override
            public void disconnected(DisconnectedEvent event) {
                Logger.log(Logger.LOG_CRIT, "Server disconnected: " + Message.fromString(event.getReason()).getFullText());
                System.exit(1);
            }
        });

        // Send command
        client.getSession().send(new ClientChatPacket(command));

        int cur_loop = 0; // Current loop number
        while (response.equals("") && cur_loop < Scanner.timeout) {
            try {
                Thread.sleep(1); // Sleep 1MS
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cur_loop++;
        }

        if (!response.equals("") && !response.equals("\"\"")) {
            remove_listeners();
            String tmp_response = response;
            response = ""; // Clear response
            //System.out.println("\n\nCommand: " + command + ". Response: " + tmp_response); // DEBUG
            //System.out.println("Listeners: " + Integer.toString(client.getSession().getListeners().size()));
            return tmp_response;
        }

        // Failure
        response = "";
        Scanner.errors++; // Increment errors
        remove_listeners();
        return ""; // :( Error
    }
}
