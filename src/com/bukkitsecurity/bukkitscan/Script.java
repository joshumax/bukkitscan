package com.bukkitsecurity.bukkitscan;

import java.util.*;
import java.io.*;
import java.lang.Integer;
import au.com.bytecode.opencsv.CSVReader;

/**
 * Script-handling functions
 */
public class Script {
    private File script_dir = new File("data/");
    private List<String> loaded_scripts = new ArrayList<String>();

    // Check if loaded
    private Boolean loaded = false;

    // Returns loaded_scripts to caller
    public List<String> getLoadedScripts() {
        return loaded_scripts;
    }

    // Sorts the loaded_scripts from least to greatest
    // TODO: Create a better implementation
    private void sortScripts() {
        // Iterate and sort scripts
        int counter = 0;
        for (int i = 0; i < loaded_scripts.size() - 1; i++) {
            // All sorted if true!
            if (counter == loaded_scripts.size())
                break;

            // Get pivots
            String[] pivot_first = loaded_scripts.get(i).split("_");
            String[] pivot_second = loaded_scripts.get(i+1).split("_");

            // Sort
            if (Integer.parseInt(pivot_first[0]) > Integer.parseInt(pivot_second[0])) {
                String first_old = loaded_scripts.get(i);
                String second_old = loaded_scripts.get(i+1);

                // Re-order
                loaded_scripts.set(i, second_old);
                loaded_scripts.set(i+1, first_old);

                // Reset counter
                counter = 0;

                // Retry
                i = 0;
            } else {
                // Increment counter
                counter++;
                continue;
            }
        }
    }

    // Adds or removes various scripts from loaded_scripts
    private void addRemoveScripts(List<String> args) {
        System.out.println("\n== Customizing Scan ==");
        // Loop through the arg list
        for (int i = 0; i < args.size(); i++) {
            int argnumval;
            // Check if the first bit is a number
            try {
                String[] bit = args.get(i).split("_");
                argnumval = Integer.parseInt(bit[0]);
            } catch (NumberFormatException e) {
                // Error!
                System.out.println("Error: argument(s) does not contain a number!");
                continue;
            }

            // We want to remove a script
            if (argnumval < 100 && argnumval >= 0) {
                // Iterate through loaded_scripts list and remove if found
                for (int j = 0; j < loaded_scripts.size(); j++) {
                    if (loaded_scripts.get(j).equals(args.get(i) + ".script")) {
                        // Remove from loaded_scripts
                        loaded_scripts.remove(j);
                        break;
                    }
                }
            }

            // We want to add a script
            if (argnumval > 100) {
                // Add to loaded_scripts
                File f = new File(script_dir.toPath() + "/" + args.get(i) + ".script");
                // Check if the script exists first
                if (f.exists()) {
                    loaded_scripts.add(args.get(i) + ".script");
                } else {
                    System.out.println(f.toPath() + ": file not found!");
                }
            }
        }
    }

    // Get all scripts with a beginning name of < 100
    private void getScripts(final File directory) {
        for (final File fileEntry : directory.listFiles()) {
            if (!fileEntry.isDirectory()) {
                // Split name from number
                int scriptnum;
                String[] splitfile = fileEntry.getName().split("_");
                try {
                    scriptnum = Integer.parseInt(splitfile[0]);
                } catch (NumberFormatException e) {
                    System.out.println("Ignoring " + splitfile[0]);
                    continue;
                }
                // Add the file to loaded scripts
                if (splitfile.length > 1 && scriptnum < 100) {
                    loaded_scripts.add(splitfile[0] + "_" + splitfile[1]);
                }
            }
        }
    }

    // Load and sort the scripts from the script directory
    public int load(List<String> enable_disable) {
        // Check to see if the directory exists
        if (!script_dir.exists() || !script_dir.isDirectory()) {
            // Not found
            System.out.println("Error: Script directory not found!");
            System.exit(1);
        }

        // Get all files in the directory
        System.out.println("Collecting scripts...");
        this.getScripts(script_dir);

        // Add-or remove scripts if args say so
        if (enable_disable.size() > 0)
            this.addRemoveScripts(enable_disable);

        // Sort scripts
        this.sortScripts();

        // Show loaded scripts
        System.out.println("\nLoaded scripts...");
        for (int i = 0; i < loaded_scripts.size(); i++)
             System.out.println(loaded_scripts.get(i));
        System.out.println("");

        // Set the loaded flag to true
        loaded = true;

        return 0;
    }

    // Check a line of the csv array to make sure it's okay
    private Boolean checkCSV(String[] line) {
       /* CSV line layout:
        * name,command,valid_response,invalid_response,severity(0-3),description,version(not 0)
        */

        // Array length should be 7
        if (line.length != 7)
            return false;

        // "name" should contain something
        if (line[0].equals(""))
            return false;

        // "command" should contain "/"
        if (!line[1].matches("/(.*)"))
            return false;

        // "valid_response" should contain something
        if (line[2].equals(""))
            return false;

        // "invalid_response" should contain something
        if (line[3].equals(""))
            return false;

        // "severity" should contain 0-3
        if (!line[4].matches("[0-3]"))
            return false;

        // "description" should contain something
        if (line[5].equals(""))
            return false;

        // "version" should contain something
        if (line[6].equals(""))
            return false;

        // Checks passed :)
        return true;
    }

    // Run the scanner with the current supplied list entry (called from run())
    private void run_scan(Scanner scanner, String entry) {
        Logger.log(Logger.LOG_INFO, "Running " + entry);

        // Start reader
        try {
            CSVReader reader = new CSVReader(new FileReader(script_dir.toPath() + "/" + entry));
            String [] nextLine;
            int current_line = 0;
            while ((nextLine = reader.readNext()) != null) {

                // Increment current line number
                current_line++;

                // Check to see if it is a CSV comment "#"
                if (nextLine[0].matches("#(.*)"))
                    continue;

                // nextLine[] is an array of values from the line
                if (this.checkCSV(nextLine)) {
                    // Parse okay, do scan :)
                    scanner.scan(nextLine[0], nextLine[1],
                            nextLine[2], nextLine[3],
                            nextLine[4], nextLine[5], nextLine[6]);
                } else {
                    // Parse error :(
                    Logger.log(Logger.LOG_CRIT, entry + ": CSV Parse error at line " + current_line);
                    System.exit(1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Logger.log(Logger.LOG_CRIT, "Filesystem I/O error!");

            // Die
            System.exit(1);
        }
        Logger.log(Logger.LOG_INFO, "Script completed!");
    }

    // Run scanner with supplied scripts (use this after load)
    public void run(Scanner scanner) {
        // Check to see if load() has been called
        if (!loaded) {
            System.out.println("run() before load()");
            System.exit(1);
        }

        // Loop through all of the loaded scripts and run them
        for (int i = 0; i < loaded_scripts.size(); i++)
            this.run_scan(scanner, loaded_scripts.get(i));

        // All done with the scan!
        System.out.println("\nScan scripts completed with " + Integer.toString(Scanner.errors) + " errors!");
    }
}
